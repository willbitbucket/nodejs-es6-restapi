import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import {plugin} from 'mongoose-diff-history/diffHistory';

/**
 * Asset: Name, physical dimensions, associated Shopping Centre,
  location within the centre, and status
 */


const AssetSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  dimensions: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  shoppingCentre: {type: mongoose.Schema.Types.ObjectId, ref: 'ShoppingCentre'},
  createdAt: {
    type: Date,
    default: Date.now
  },
  status:{
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
  },
  updatedOn: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

AssetSchema.plugin(plugin);

/*
hooks for data integrity
*/
AssetSchema.pre('save', function(next){
  let ShoppingCentre = mongoose.model('ShoppingCentre'); //--> add this line
  return ShoppingCentre.findById(this.shoppingCentre)
  .then(doc => {
    if (doc) return next();
    else throw Error("not found");})
  .catch( e => {
    const err = new APIError('No such shoppingCenter exists!', httpStatus.NOT_FOUND);
    return next(err);
  });
});

AssetSchema.post('save', function(doc) {
  //simplify to only have one side maitain the one-to-many relationships
});

/**
 * Methods
 */
AssetSchema.method({
});

/**
 * Statics
 */
AssetSchema.statics = {
  get(id) {
    return this.findById(id)
      .exec()
      .then((doc) => {
        if (doc) {
          return doc;
        }
        const err = new APIError('No such asset exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List documents in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<Asset[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef Asset
 */
export default mongoose.model('Asset', AssetSchema);
