import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import {plugin} from 'mongoose-diff-history/diffHistory';

/**
 * Shopping centre: Name, address, and have Assets
 */
const ShoppingCentreSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  address: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

ShoppingCentreSchema.plugin(plugin);

/**
 * Methods
 */
ShoppingCentreSchema.method({
});

/**
 * Statics
 */
ShoppingCentreSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<ShoppingCentre, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then( doc  => {
        if (doc) {
          return doc;
        }
        const err = new APIError('No such shopping centre exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List documents in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<ShoppingCentre[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef ShoppingCentre
 */
export default mongoose.model('ShoppingCentre', ShoppingCentreSchema);
