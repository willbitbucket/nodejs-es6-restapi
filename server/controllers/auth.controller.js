import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/config';

// sample user, used for authentication
const user = {
  username: 'admin',
  password: 'admin'
};
const user2 = {
  username: 'support',
  password: 'support'
};
/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function login(req, res, next) {
  // Ideally you'll fetch this from the db
  // Idea here was to show how jwt works with simplicity
  if ((req.body.username === user.username && req.body.password === user.password)
  ||(req.body.username === user2.username && req.body.password === user2.password)) {
      const token = jwt.sign({
      username: req.body.username
    }, config.jwtSecret);
    return res.json({
      token,
      username: req.body.username
    });
  }

  const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
  return next(err);
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

export default { login, getRandomNumber };
