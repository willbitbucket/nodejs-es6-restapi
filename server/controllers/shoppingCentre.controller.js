import ShoppingCentre from '../models/shoppingCentre.model';
import {auditModel} from '../helpers/audit';
import diffHistory from "mongoose-diff-history/diffHistory";

/**
 * Load shoppingCentre and append to req.
 */
function load(req, res, next, id) {
  ShoppingCentre.get(id)
    .then((shoppingCentre) => {
      req.shoppingCentre = shoppingCentre; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get shoppingCentre
 * @returns {ShoppingCentre}
 */
function get(req, res) {
  return res.json(req.shoppingCentre);
}

/**
 * Create new shoppingCentre
 * @property {string} req.body.shoppingCentrename - The shoppingCentrename of shoppingCentre.
 * @property {string} req.body.mobileNumber - The mobileNumber of shoppingCentre.
 * @returns {ShoppingCentre}
 */
function create(req, res, next) {
  const shoppingCentre = new ShoppingCentre({
    name: req.body.name,
    address: req.body.address
  });

  auditModel(shoppingCentre, req.user);

  shoppingCentre.save()
    .then(savedShoppingCentre => res.json(savedShoppingCentre))
    .catch(e => next(e));
}

/**
 * Update existing shoppingCentre
 * @property {string} req.body.name - The name of shoppingCentre.
 * @property {string} req.body.address - The address of shoppingCentre.
 * @returns {ShoppingCentre}
 */
function update(req, res, next) {
  let shoppingCentre = req.shoppingCentre;

  Object.assign(shoppingCentre, req.body)

  auditModel(shoppingCentre, req.user);

  return shoppingCentre.save()
    .then(savedShoppingCentre => res.json(savedShoppingCentre))
    .catch(e => next(e));
}

/**
 * Get shoppingCentre list.
 * @property {number} req.query.skip - Number of shoppingCentres to be skipped.
 * @property {number} req.query.limit - Limit number of shoppingCentres to be returned.
 * @returns {ShoppingCentre[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  ShoppingCentre.list({ limit, skip })
    .then(shoppingCentres => res.json(shoppingCentres))
    .catch(e => next(e));
}

/**
 * Delete shoppingCentre.
 * @returns {ShoppingCentre}
 */
function remove(req, res, next) {
  const shoppingCentre = req.shoppingCentre;

  auditModel(shoppingCentre, req.user);

  shoppingCentre.remove()
    .then(deletedShoppingCentre => res.json(deletedShoppingCentre))
    .catch(e => next(e));
}

function getAudit(req, res, next) {
       diffHistory.getHistories("ShoppingCentre", req.params.shoppingCentreId, ["name", "address"], function (err, histories) {
             if (err){ return next(err);}
             res.json(histories);
         })

}

export default { load, get, create, update, list, remove, getAudit };
