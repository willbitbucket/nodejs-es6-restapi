import Asset from '../models/asset.model';
import { auditModel } from '../helpers/audit';
import diffHistory from "mongoose-diff-history/diffHistory";

/**
 * Load asset and append to req.
 */
function load(req, res, next, id) {
  Asset
    .get(id)
    .then((asset) => {
      req.asset = asset; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get asset
 * @returns {Asset}
 */
function get(req, res, next) {
  Asset
    .findById(req.asset._id)
    .populate('shoppingCentre')
    .exec()
    .then((asset) => {
      req.asset = asset; // eslint-disable-line no-param-reassign
      res.json(req.asset);
    })
    .catch( e => next(e))
}

/**
 * Create new asset
 */
function create(req, res, next) {
  const asset = new Asset({
    name: req.body.name,
    location: req.body.location,
    dimensions: req.body.dimensions,
    shoppingCentre: req.body.shoppingCentre,
  });

  auditModel(asset, req.user);

  asset.save()
    .then(savedAsset => res.json(savedAsset))
    .catch(e => next(e));
}

/**
 * Update existing asset
 */
function update(req, res, next) {
  const asset = req.asset;

  Object.assign(asset, req.body)

  auditModel(asset, req.user);

  return asset.save()
    .then(savedAsset => res.json(savedAsset))
    .catch(e => next(e));
}

/**
 * Get asset list.
 * @property {number} req.query.skip - Number of assets to be skipped.
 * @property {number} req.query.limit - Limit number of assets to be returned.
 * @returns {Asset[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Asset.list({ limit, skip })
    .then(assets => res.json(assets))
    .catch(e => next(e));
}

function search(req, res, next) {

  let { limit = 50, skip = 0, name, shoppingCentre, status } = req.query;
  limit = parseInt(limit);
  skip = parseInt(skip);

  let condition = {};
  if (name) condition.name = new RegExp(name);
  if (shoppingCentre) condition.shoppingCentre = shoppingCentre;
  if (status) condition.status = status;

  Asset.
    find(condition)
    .populate('shoppingCentre')
    .
    skip(skip).
    limit(limit).
    exec().
    then((assets) => {
      res.json(assets);
    })
    .catch( e => next(e))
}

/**
 * Delete asset.
 * @returns {Asset}
 */
function remove(req, res, next) {
  const asset = req.asset;

  auditModel(asset, req.user);

  asset.remove()
    .then(deletedAsset => res.json(deletedAsset))
    .catch(e => next(e));
}


function getAudit(req, res, next) {
  const asset = req.asset;
       diffHistory.getHistories("Asset", req.params.assetId, ["name", "dimensions", "location"], function (err, histories) {
             if (err){ return next(err);}
             res.json(histories);
         })

}

export default { load, get, create, update, list, remove, getAudit, search };
