import mongoose from 'mongoose';
import chai, { expect } from 'chai';
import app from '../../index';

import sinon  from 'sinon';

require('sinon-mongoose');



import assetCtrl from '../controllers/asset.controller'

const asset1 = {
        "_id": "5a23803c631cc202d43c705c",
        "name": "www",
        "location": "lllllll",
        "dimensions": "1111111",
        "shoppingCentre": {
            "_id": "5a22233d0d1af058522cdbdd",
            "name": "westfield chatswood2",
            "address": "1 dsd street",
            "assets": [],
            "__v": 0,
            "createdAt": "2017-12-02T03:51:25.335Z"
        },
        "__v": 0,
        "updatedOn": "2017-12-03T04:40:28.210Z",
        "status": "active",
        "createdAt": "2017-12-03T04:40:28.210Z"
    };

    let Asset,AssetMock, asset, assetMock;
describe('## Asset API unit test', function() {
  beforeEach(function () {
      Asset = require('../models/asset.model');
      AssetMock = sinon.mock(Asset);

    });
    describe('# search to return mock assets', function () {
        it("returns mocked results", function (done) {
          const reqSearch = {query:{name: 'paper', shoppingCentre:'1234', status: 'active'}};

          AssetMock
              .expects('find').withArgs({name: new RegExp('paper'), shoppingCentre:'1234', status: 'active'})
              .chain('limit', 50)
              .chain('skip', '10')
              .chain('exec')
              .resolves(asset1);
          assetCtrl.search(reqSearch, (doc) => {assert.equal(asset1, doc)}, () => {});
          done();
        }
      );

      describe('# get to return mock asset', function () {
          it("returns mocked results", function (done) {
            const req = {asset:{_id: 'id'}};

            AssetMock
                .expects('findById').withArgs('id')
                .chain('populate')
                .chain('exec')
                .resolves(asset1);
            assetCtrl.get(req, (doc) => {assert.equal(asset1, doc)}, () => {});
            done();
          });
        });

        describe('# update to return mock assets', function () {
            it("returns updated mocked results", function (done) {
              assetMock = new Asset({
                "name": "www",
                "location": "lllllll",
                "dimensions": "1111111",
                "status":"active"});
              const req =   {
                "body":{
                	"status":"inactive"
                },
                  "asset":assetMock
                };
              const expectAsset = {
                "name": "www",
                "location": "lllllll",
                "dimensions": "1111111",
                "status":"inactive"
              };
              let stub = sinon.stub(assetMock, 'save');
              stub.returns(new Promise( (r, j) =>{r(expectAsset);}));

              assetCtrl.update(req, (doc) => {assert.equal(expectAsset, doc)}, () => {});
              done();
            });
          });

    afterEach(()=>{
      
    })

  })
})
