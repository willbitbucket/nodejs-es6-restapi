import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import ctrl from '../controllers/asset.controller';

import config from '../../config/config';
import expressJwt from 'express-jwt';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/assets - Get list of assets */
  .get(ctrl.search)

  /** POST /api/assets - Create new user */
  .post(validate(paramValidation.createAsset),
        expressJwt({ secret: config.jwtSecret }),
        ctrl.create);

  router.route('/:id')
    /** GET /api/assets/:id - Get user */
    .get(ctrl.get)

    /** PUT /api/assets/:id - Update user */
    .put(validate(paramValidation.updateAsset),
        expressJwt({ secret: config.jwtSecret }),
        ctrl.update)

    /** DELETE /api/assets/:id - Delete user */
    .delete(expressJwt({ secret: config.jwtSecret }), ctrl.remove);

    router.route('/:assetId/audit')
      /** GET /api/assets/:id - Get user */
      .get(ctrl.getAudit)

    /** Load user when API with id route parameter is hit */
    router.param('id', ctrl.load);

export default router;
