import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import ctrl from '../controllers/shoppingCentre.controller';

import config from '../../config/config';
import expressJwt from 'express-jwt';

const router = express.Router(); // eslint-disable-line new-cap



router.route('/')
  /** GET /api/shoppingcentres - Get list of shoppingcentres */
  .get(ctrl.list)

  /** POST /api/shoppingcentres - Create new user */
  .post(validate(paramValidation.createShoppingCentre),
    expressJwt({ secret: config.jwtSecret }),
    ctrl.create);

  router.route('/:id')
    /** GET /api/shoppingcentres/:id - Get user */
    .get(ctrl.get)

    /** PUT /api/shoppingcentres/:id - Update user */
    .put(validate(paramValidation.updateShoppingCentre),
      expressJwt({ secret: config.jwtSecret }),
      ctrl.update)

    /** DELETE /api/shoppingcentres/:id - Delete user */
    .delete(expressJwt({ secret: config.jwtSecret }), ctrl.remove);


    router.route('/:shoppingCentreId/audit')
      /** GET /api/assets/:id - Get user */
      .get(ctrl.getAudit)

    /** Load user when API with userId route parameter is hit */
    router.param('id', ctrl.load);

export default router;
