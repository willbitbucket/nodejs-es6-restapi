import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  },
  /**
   * Shopping centre: Name, address, and have Assets
   */
  // POST /api/shoppingcentres
  createShoppingCentre: {
    body: {
      name: Joi.string().required(),
      address: Joi.string().required()
    }
  },
  updateShoppingCentre: {
    body: {
      name: Joi.string().optional(),
      address: Joi.string().optional()
    },
    params: {
      id: Joi.string().hex().required()
    }
  },
  /**
   * Asset: Name, physical dimensions, associated Shopping Centre,
    location within the centre, and status
   */
   createAsset: {
     body: {
       name: Joi.string().required(),
       dimensions: Joi.string().required(),
       location: Joi.string().required(),
       shoppingCentreId: Joi.string().hex().optional()
     }
   },
   updateAsset: {
     body: {
       name: Joi.string().optional(),
       dimensions: Joi.string().optional(),
       location: Joi.string().optional(),
       shoppingCentre: Joi.string().hex().optional(),
       status: Joi.string().optional(),
     },
     params: {
       id: Joi.string().hex().required()
     }
   },
};
