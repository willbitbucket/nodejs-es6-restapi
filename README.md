## Requirement:

```
Shopping centre: Name, address, and have Assets
A Shopping Centre is a mall like MYER or Westfield, with many stores
operating inside; placed at key locations within the Shopping Centre are Assets,
which are oOh! Media units for displaying content. Shopping Centres must at a
minimum have the attributes: Name, address, and have Assets associated with
them.


Asset: Name, physical dimensions, associated Shopping Centre, location within the centre, and status

An Asset is a physical screen which receives advertisement and other content
throughout the day. It has physical attributes such as itʼs dimensions, a location
within the shopping centre, and a status indicating whether it is active to
receive content or offline for maintenance. Assets must at a minimum have the
attributes: Name, physical dimensions, associated Shopping Centre, location
within the centre, and status.

Your solution
MUST have an API server written in Javascript
MUST have routes for Shopping Centres
MUST have routes for Assets
MUST persist data to a database
SHOULD be secured against anonymous access and track which user
makes changes to the data
SHOULD allow marking Assets “inactive” for when theyʼre receiving
maintenance, and re-activating them later
COULD have a UI (but donʼt worry about UX)
COULD support searching for Assets by Name, Shopping Centre, or Status
```

## Backend Solution :

### Ndoejs Express + Mongoose + JWT

 1. audits on assets and shoppingCentre (no UI for this yet), via mongoose-diff-history, which will insert audit logs into a sperate collection. (get http://localhost:4050/api/assets/{id}/audit)
 2. jwt for security (post http://localhost:4050/api/auth/login), get calls are open for un-logged user, but modification requires login, via login page.
 3. search supports  shoppingCentre, status and regex for name (get http://localhost:4050/api/assets?name=xyz&status=active)
 4. CURD via UI only supports Asset, while ShoppingCentre needs add via API (post/put http://localhost:4050/api/shoppingCentres)
 5. default user/pass to see the audit for different users: admin/admin or support/support

### Run:
```
start mongo locally
npm install
npm start
```


### Test:
```
npm test
```

### TODO:
1. more tests are needed due to limited of time.
2. searching by shoppingCentre name instead of the id


### Have problem to manage dependency?
In case npm install still miss dependency, do followings:
```
npm install -g npm-install-missing
npm-install-missing
```
